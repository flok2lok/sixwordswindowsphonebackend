﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Web.Http;

namespace LearnLanguagesWindowsBackend.Controllers
{
  public class ValuesController : ApiController
  {
    SqlCommand command;

    public ValuesController()
    {
      command = Pusher.command;
    }

    public async void Post()
    {
      NameValueCollection data = await Request.Content.ReadAsFormDataAsync();
      string id = data["id"];
      string lang = data["lang"];
      lock (command)
      {
        if (!String.IsNullOrEmpty(id) && !String.IsNullOrEmpty(id))
        {
          command.CommandText = "select top 1 URL from PushTable where URL=@URL;";
          command.Parameters.AddParameterWithTypeAndValue("@URL", SqlDbType.VarChar, id);
          string existValue = null;
          try
          {
            using (var reader = command.ExecuteReader())
            {
              while (reader.Read())
                existValue = reader["URL"].ToString();
            }
          }
          catch { }

          if (existValue != null)
            command.CommandText = "update top (1) PushTable set TS=@TS, REGION=@REGION where URL=@URL;";
          else
            command.CommandText = "insert PushTable (URL,TS,REGION) values (@URL,@TS,@REGION);";

          command.Parameters.AddParameterWithTypeAndValue("@URL", SqlDbType.VarChar, id);
          command.Parameters.AddParameterWithTypeAndValue("@REGION", SqlDbType.VarChar, lang);
          command.Parameters.AddParameterWithTypeAndValue("@TS", SqlDbType.Int, (DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds + Pusher.Updates[0]);

          try { command.ExecuteNonQuery(); }
          catch { }
        }
      }
    }

    public async void Delete()
    {
      NameValueCollection data = await Request.Content.ReadAsFormDataAsync();
      string id = data["id"];
      lock (command)
      {
        command.CommandText = "delete top (1) PushTable where URL = @A1;";
        command.Parameters.AddParameterWithTypeAndValue("@A1", SqlDbType.VarChar, id);
        try { command.ExecuteNonQuery(); }
        catch { }
      }
    }
  }
}