﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;

namespace LearnLanguagesWindowsBackend
{
  static class Pusher
  {
    public static int[] Updates = new int[] { 172800, 604800, 1209600 }; // 2 days, 1 week, 2 weeks

    static Tuple<string, string, string>[] Values = new Tuple<string, string, string>[]
      {
        new Tuple<string,string,string>("en", "6 words", "Don't forget to learn words!"),
        new Tuple<string,string,string>("fr", "6 mots", "Pas oubliez d'apprendre les mots!"),
        new Tuple<string,string,string>("ru", "6 слов", "Не забывайте учить слова!"),
        new Tuple<string,string,string>("it", "6 parole", "Non dimenticare imparare le parole!"),
        new Tuple<string,string,string>("de", "6 wörter", "Vergessen Sie nicht Worte lernen!"),
        new Tuple<string,string,string>("es"," 6 palabras", "No te olvides de aprender las palabras!")
      };

    public static SqlConnection connection;
    public static SqlCommand command;
    public static Timer timer;

    static Pusher()
    {
      connection = new SqlConnection(ConfigurationManager.ConnectionStrings["AzureConnectionString"].ConnectionString);
      connection.Open();
      command = new SqlCommand();
      command.Connection = connection;

      timer = new Timer(1800000);
      timer.AutoReset = true;
      timer.Elapsed += timer_Elapsed;
      timer.Start();
    }

    static public void Init() { }

    static void timer_Elapsed(object sender, ElapsedEventArgs e)
    {
      List<Tuple<string, int>> UpdateList = new List<Tuple<string, int>>();
      List<string> DeleteList = new List<string>();
      List<Tuple<string, string>> SendList = new List<Tuple<string, string>>();

      lock (command)
      {
        command.Parameters.Clear();
        command.Parameters.AddParameterWithTypeAndValue("@TS", SqlDbType.Int, (DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds);
        command.CommandText = "select * from PushTable where TS<@TS;";

        using (var reader = command.ExecuteReader())
        {
          while (reader.Read())
          {
            string subscriptionUri = reader["URL"].ToString();

            int count = int.Parse(reader["COUNT"].ToString());
            if (count < Updates.Length)
              UpdateList.Add(new Tuple<string, int>(subscriptionUri, Updates[count]));
            else
              DeleteList.Add(subscriptionUri);

            string lang = reader["REGION"].ToString();
            var tuple = Values.First(x => x.Item1 == lang);
            string toastMessage = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<wp:Notification xmlns:wp=\"WPNotification\">" +
               "<wp:Toast>" +
                    "<wp:Text1>" + tuple.Item2 + "</wp:Text1>" +
                    "<wp:Text2>" + tuple.Item3 + "</wp:Text2>" +
                    "<wp:Param>/MainPage.xaml</wp:Param>" +
               "</wp:Toast> " +
            "</wp:Notification>";

            SendList.Add(new Tuple<string, string>(subscriptionUri, toastMessage));
          }
        }
      }

      foreach (var send in SendList)
      {
        try
        {
          HttpWebRequest sendNotificationRequest = (HttpWebRequest)WebRequest.Create(send.Item1);
          sendNotificationRequest.Method = "POST";

          byte[] notificationMessage = Encoding.UTF8.GetBytes(send.Item2);
          sendNotificationRequest.ContentLength = notificationMessage.Length;
          sendNotificationRequest.ContentType = "text/xml";
          sendNotificationRequest.Headers.Add("X-WindowsPhone-Target", "toast");
          sendNotificationRequest.Headers.Add("X-NotificationClass", "2");
          using (Stream requestStream = sendNotificationRequest.GetRequestStream())
          {
            requestStream.Write(notificationMessage, 0, notificationMessage.Length);
          }
          HttpWebResponse response = (HttpWebResponse)sendNotificationRequest.GetResponse();
          string notificationStatus = response.Headers["X-NotificationStatus"];
          string notificationChannelStatus = response.Headers["X-SubscriptionStatus"];
          string deviceConnectionStatus = response.Headers["X-DeviceConnectionStatus"];
        }
        catch { }
      }

      foreach (var tuple in UpdateList)
      {
        lock (command)
        {
          command.Parameters.AddParameterWithTypeAndValue("@ADD", SqlDbType.Int, tuple.Item2);
          command.Parameters.AddParameterWithTypeAndValue("@URL", SqlDbType.VarChar, tuple.Item1);
          command.CommandText = "update top (1) PushTable set TS=TS+@ADD, COUNT=COUNT+1 where URL=@URL";
          command.ExecuteNonQuery();
        }
      }

      foreach (var delete in DeleteList)
      {
        lock (command)
        {
          command.Parameters.AddParameterWithTypeAndValue("@URL", SqlDbType.VarChar, delete);
          command.CommandText = "delete top (1) PushTable where URL=@URL";
          command.ExecuteNonQuery();
        }
      }
    }
  }
}