﻿using System.Data;
using System.Data.SqlClient;

namespace LearnLanguagesWindowsBackend
{
  public static class SqlParameterCollectionExtension
  {
    public static void AddParameterWithTypeAndValue(this SqlParameterCollection col, string name, SqlDbType type, object value)
    {
      if (col.Contains(name))
        col.Remove(col[name]);
      col.Add(name, type);
      col[name].Value = value;
    }
  }
}